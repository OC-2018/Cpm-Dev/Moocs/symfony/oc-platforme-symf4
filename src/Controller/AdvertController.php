<?php
/**
 * Created by PhpStorm.
 * User: joel2608
 * Date: 13/08/2018
 * Time: 20:01
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;   // For TWIG
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdvertController
 * @package App\Controller
 */
class AdvertController extends AbstractController
{
    /**
     * Index Action
     *
     * @param $page
     *
     * @return Response
     */
    public function index($page) {

        // On ne sait pas combien de page il y a
        // Mais on sait qu'une page doit être >= 1
        if ($page < 1) {
            throw new NotFoundHttpException('page "' . $page . '" inexistante !'):
        }

        // Récupère la liste des annonnces

        // Mais pour l'instant on ne fat qu'appeller le template

        return $this->render('advert/index.html.twig', array(
            'app_name' => 'Exchange Application'
        ));

    }

    /**
     * Vew Action
     *
     * @param $id
     *
     * @return Response
     */
    public function view($id, Request $request)
    {
        //  Ici on récupèrera l'annonce correspondant à lid $id

        return $this->render('advert/view.html.twig', array(
            'id' => $id
        ));
    }

    /**
     * Add Action
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request)
    {
        // Si la requete est en POST; c'est que le visiteur à soumis le formulaire
        if ($request->isMethod('POST'))  {
            // Création et Gestion du formulaire

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.') ;

            // Puis on redirige vers la page de visualisation de cette annonce
            return $this->redirectToRoute('oc_platform_view', array('id' => 5));
        }

        // Si on est pas en POST
        return $this->render('advert/add.html.twig');
    }

    /**
     * Edit action
     *
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit($id, Request $request)
    {
      // On recupère l'annonce d'id $id

        // Meme fonctionnement que pour l'ajout
        if ($request->isMethod('POST'))
        {
            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien modifiée.') ;

            // Puis on redirige vers la page de visualisation de cette annonce
            return $this->redirectToRoute('oc_platform_view', array('id' => 5));
        }

        // Si on est pas en POST
        return $this->render('advert/edit.html.twig');
    }

    /**
     * Delete Action
     *
     * @param $id
     *
     * @return Response
     */
    public function delete($id)
    {
        // On recupère l'annonce d'id $id

        // on gère la suppression de l'annonce en question

        return $this->render('advert/delete.html.twig');
    }
}